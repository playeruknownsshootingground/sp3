#include "SceneText.h"
#include "GL\glew.h"

#include "shader.hpp"
#include "MeshBuilder.h"
#include "Application.h"
#include "Utility.h"
#include "LoadTGA.h"
#include <sstream>
#include <string>

SceneText::SceneText()
{
}

SceneText::~SceneText()
{
}

void SceneText::Init()
{
	// Black background
	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);
	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS); 
	
	glDisable(GL_CULL_FACE);
	
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glGenVertexArrays(1, &m_vertexArrayID);
	glBindVertexArray(m_vertexArrayID);

	//Shadow Uniform
	m_gPassShaderID = LoadShaders("Shader//GPass.vertexshader","Shader//GPass.fragmentshader");
	m_parameters[U_LIGHT_DEPTH_MVP_GPASS] = glGetUniformLocation(m_gPassShaderID, "lightDepthMVP");

	m_programID = LoadShaders("Shader//Shadow.vertexshader","Shader//Shadow.fragmentshader");
	m_parameters[U_LIGHT_DEPTH_MVP] = glGetUniformLocation(m_programID, "lightDepthMVP");
	m_parameters[U_SHADOW_MAP] = glGetUniformLocation(m_programID, "shadowMap");

	// Get a handle for our uniform
	m_parameters[U_MVP] = glGetUniformLocation(m_programID, "MVP");
	m_parameters[U_MODELVIEW] = glGetUniformLocation(m_programID, "MV");
	m_parameters[U_MODELVIEW_INVERSE_TRANSPOSE] = glGetUniformLocation(m_programID, "MV_inverse_transpose");
	m_parameters[U_MATERIAL_AMBIENT] = glGetUniformLocation(m_programID, "material.kAmbient");
	m_parameters[U_MATERIAL_DIFFUSE] = glGetUniformLocation(m_programID, "material.kDiffuse");
	m_parameters[U_MATERIAL_SPECULAR] = glGetUniformLocation(m_programID, "material.kSpecular");
	m_parameters[U_MATERIAL_SHININESS] = glGetUniformLocation(m_programID, "material.kShininess");
	m_parameters[U_LIGHTENABLED] = glGetUniformLocation(m_programID, "lightEnabled");
	m_parameters[U_NUMLIGHTS] = glGetUniformLocation(m_programID, "numLights");
	m_parameters[U_LIGHT0_TYPE] = glGetUniformLocation(m_programID, "lights[0].type");
	m_parameters[U_LIGHT0_POSITION] = glGetUniformLocation(m_programID, "lights[0].position_cameraspace");
	m_parameters[U_LIGHT0_COLOR] = glGetUniformLocation(m_programID, "lights[0].color");
	m_parameters[U_LIGHT0_POWER] = glGetUniformLocation(m_programID, "lights[0].power");
	m_parameters[U_LIGHT0_KC] = glGetUniformLocation(m_programID, "lights[0].kC");
	m_parameters[U_LIGHT0_KL] = glGetUniformLocation(m_programID, "lights[0].kL");
	m_parameters[U_LIGHT0_KQ] = glGetUniformLocation(m_programID, "lights[0].kQ");
	m_parameters[U_LIGHT0_SPOTDIRECTION] = glGetUniformLocation(m_programID, "lights[0].spotDirection");
	m_parameters[U_LIGHT0_COSCUTOFF] = glGetUniformLocation(m_programID, "lights[0].cosCutoff");
	m_parameters[U_LIGHT0_COSINNER] = glGetUniformLocation(m_programID, "lights[0].cosInner");
	m_parameters[U_LIGHT0_EXPONENT] = glGetUniformLocation(m_programID, "lights[0].exponent");
	m_parameters[U_LIGHT1_TYPE] = glGetUniformLocation(m_programID, "lights[1].type");
	m_parameters[U_LIGHT1_POSITION] = glGetUniformLocation(m_programID, "lights[1].position_cameraspace");
	m_parameters[U_LIGHT1_COLOR] = glGetUniformLocation(m_programID, "lights[1].color");
	m_parameters[U_LIGHT1_POWER] = glGetUniformLocation(m_programID, "lights[1].power");
	m_parameters[U_LIGHT1_KC] = glGetUniformLocation(m_programID, "lights[1].kC");
	m_parameters[U_LIGHT1_KL] = glGetUniformLocation(m_programID, "lights[1].kL");
	m_parameters[U_LIGHT1_KQ] = glGetUniformLocation(m_programID, "lights[1].kQ");
	m_parameters[U_LIGHT1_SPOTDIRECTION] = glGetUniformLocation(m_programID, "lights[1].spotDirection");
	m_parameters[U_LIGHT1_COSCUTOFF] = glGetUniformLocation(m_programID, "lights[1].cosCutoff");
	m_parameters[U_LIGHT1_COSINNER] = glGetUniformLocation(m_programID, "lights[1].cosInner");
	m_parameters[U_LIGHT1_EXPONENT] = glGetUniformLocation(m_programID, "lights[1].exponent");
	// Get a handle for our "colorTexture" uniform
	m_parameters[U_COLOR_TEXTURE_ENABLED] = glGetUniformLocation(m_programID, "colorTextureEnabled[0]");
	m_parameters[U_COLOR_TEXTURE] = glGetUniformLocation(m_programID, "colorTexture[0]");
	m_parameters[U_COLOR_TEXTURE_ENABLED1] = glGetUniformLocation(m_programID, "colorTextureEnabled[1]");
	m_parameters[U_COLOR_TEXTURE1] = glGetUniformLocation(m_programID, "colorTexture[1]");

	m_parameters[U_COLOR_TEXTURE_ENABLED2] = glGetUniformLocation(m_programID, "colorTextureEnabled[2]");
	m_parameters[U_COLOR_TEXTURE2] = glGetUniformLocation(m_programID, "colorTexture[2]");
	m_parameters[U_COLOR_TEXTURE_ENABLED3] = glGetUniformLocation(m_programID, "colorTextureEnabled[3]");
	m_parameters[U_COLOR_TEXTURE3] = glGetUniformLocation(m_programID, "colorTexture[3]");
	m_parameters[U_COLOR_TEXTURE_ENABLED4] = glGetUniformLocation(m_programID, "colorTextureEnabled[4]");
	m_parameters[U_COLOR_TEXTURE4] = glGetUniformLocation(m_programID, "colorTexture[4]");
	m_parameters[U_COLOR_TEXTURE_ENABLED5] = glGetUniformLocation(m_programID, "colorTextureEnabled[5]");
	m_parameters[U_COLOR_TEXTURE5] = glGetUniformLocation(m_programID, "colorTexture[5]");
	m_parameters[U_COLOR_TEXTURE_ENABLED6] = glGetUniformLocation(m_programID, "colorTextureEnabled[6]");
	m_parameters[U_COLOR_TEXTURE6] = glGetUniformLocation(m_programID, "colorTexture[6]");
	m_parameters[U_COLOR_TEXTURE_ENABLED7] = glGetUniformLocation(m_programID, "colorTextureEnabled[7]");
	m_parameters[U_COLOR_TEXTURE7] = glGetUniformLocation(m_programID, "colorTexture[7]");

	// Get a handle for our "textColor" uniform
	m_parameters[U_TEXT_ENABLED] = glGetUniformLocation(m_programID, "textEnabled");
	m_parameters[U_TEXT_COLOR] = glGetUniformLocation(m_programID, "textColor");

	// Get a handle for our "FOG" uniform
	m_parameters[U_FOG_COLOR] = glGetUniformLocation(m_programID, "fogParam.color");
	m_parameters[U_FOG_START] = glGetUniformLocation(m_programID, "fogParam.start");
	m_parameters[U_FOG_END] = glGetUniformLocation(m_programID, "fogParam.end");
	m_parameters[U_FOG_DENSITY] = glGetUniformLocation(m_programID, "fogParam.density");
	m_parameters[U_FOG_TYPE] = glGetUniformLocation(m_programID, "fogParam.type");
	m_parameters[U_FOG_ENABLED] = glGetUniformLocation(m_programID, "fogParam.enabled");

	
	// Use our shader
	glUseProgram(m_programID);
	m_lightDepthFBO.Init(8098, 8098);

	lLights[0].type = Light::LIGHT_DIRECTIONAL;
	lLights[0].position.Set(-40, 40, -40);
	lLights[0].color.Set(1, 1, 1, 1);
	lLights[0].power = 1.0;
	lLights[0].kC = 1.f;
	lLights[0].kL = 0.01f;
	lLights[0].kQ = 0.001f;
	lLights[0].cosCutoff = cos(Math::DegreeToRadian(45));
	lLights[0].cosInner = cos(Math::DegreeToRadian(30));
	lLights[0].exponent = 3.f;
	lLights[0].spotDirection.Set(0.f, 1.f, 0.f);
	
	glUniform1i(m_parameters[U_NUMLIGHTS], 1);
	glUniform1i(m_parameters[U_TEXT_ENABLED], 0);

	glUniform1i(m_parameters[U_LIGHT0_TYPE], lLights[0].type);
	glUniform3fv(m_parameters[U_LIGHT0_COLOR], 1, &lLights[0].color.r);
	glUniform1f(m_parameters[U_LIGHT0_POWER], lLights[0].power);
	glUniform1f(m_parameters[U_LIGHT0_KC], lLights[0].kC);
	glUniform1f(m_parameters[U_LIGHT0_KL], lLights[0].kL);
	glUniform1f(m_parameters[U_LIGHT0_KQ], lLights[0].kQ);
	glUniform1f(m_parameters[U_LIGHT0_COSCUTOFF], lLights[0].cosCutoff);
	glUniform1f(m_parameters[U_LIGHT0_COSINNER], lLights[0].cosInner);
	glUniform1f(m_parameters[U_LIGHT0_EXPONENT], lLights[0].exponent);
	
	glUniform1i(m_parameters[U_LIGHT1_TYPE], lLights[1].type);
	glUniform3fv(m_parameters[U_LIGHT1_COLOR], 1, &lLights[1].color.r);
	glUniform1f(m_parameters[U_LIGHT1_POWER], lLights[1].power);
	glUniform1f(m_parameters[U_LIGHT1_KC], lLights[1].kC);
	glUniform1f(m_parameters[U_LIGHT1_KL], lLights[1].kL);
	glUniform1f(m_parameters[U_LIGHT1_KQ], lLights[1].kQ);
	glUniform1f(m_parameters[U_LIGHT1_COSCUTOFF], lLights[1].cosCutoff);
	glUniform1f(m_parameters[U_LIGHT1_COSINNER], lLights[1].cosInner);
	glUniform1f(m_parameters[U_LIGHT1_EXPONENT], lLights[1].exponent);

	//Initialize fog parameter
	Color fogColor(0.5f, 0.5f, 0.5f);
	glUniform3fv(m_parameters[U_FOG_COLOR], 1, &fogColor.r);
	glUniform1f(m_parameters[U_FOG_START], 10); 
	glUniform1f(m_parameters[U_FOG_END], 1000);
	glUniform1f(m_parameters[U_FOG_DENSITY], 0.0000f);
	glUniform1i(m_parameters[U_FOG_TYPE], 2);
	glUniform1i(m_parameters[U_FOG_ENABLED], 1);

	camera.Init(Vector3(0, 30, 10), Vector3(0, 30, 0), Vector3(0, 31, 0));

	for(int i = 0; i < NUM_GEOMETRY; ++i)
	{
		meshList[i] = NULL;
	}
	meshList[GEO_AXES] = MeshBuilder::GenerateAxes("reference");
	meshList[GEO_CROSSHAIR] = MeshBuilder::GenerateCrossHair("crosshair");
	meshList[GEO_QUAD] = MeshBuilder::GenerateQuad("quad", Color(1, 1, 1), 1.f);
	meshList[GEO_QUAD]->textureID = LoadTGA("Image//calibri.tga");
	meshList[GEO_TEXT] = MeshBuilder::GenerateText("text", 16, 16);
	meshList[GEO_TEXT]->textureID = LoadTGA("Image//calibri.tga");
	meshList[GEO_TEXT]->material.kAmbient.Set(1, 0, 0);
	meshList[GEO_RING] = MeshBuilder::GenerateRing("ring", Color(1, 0, 1), 36, 1, 0.5f);
	meshList[GEO_LIGHTBALL] = MeshBuilder::GenerateSphere("lightball", Color(1, 1, 1), 18, 36, 1.f);
	meshList[GEO_SPHERE] = MeshBuilder::GenerateSphere("sphere", Color(1, 0, 0), 18, 36, 10.f);
	meshList[GEO_CONE] = MeshBuilder::GenerateCone("cone", Color(0.5f, 1, 0.3f), 36, 10.f, 10.f);
	meshList[GEO_CONE]->material.kDiffuse.Set(0.99f, 0.99f, 0.99f);
	meshList[GEO_CONE]->material.kSpecular.Set(0.f, 0.f, 0.f);
	//OBJ
	meshList[GEO_ROWBOAT] = MeshBuilder::GenerateOBJ("Rowboat", "OBJ//rowboat.obj");
	meshList[GEO_ROWBOAT]->textureArray[0] = LoadTGA("Image//Rowboat.tga");
	meshList[GEO_PIER_PILLAR] = MeshBuilder::GenerateOBJ("PierPillar", "OBJ//pierpillar.obj");
	meshList[GEO_PIER_PILLAR]->textureArray[0] = LoadTGA("Image//Rowboat.tga");
	meshList[GEO_PIER] = MeshBuilder::GenerateQuad("GEO_PIER", Color(1, 1, 1), 1.f);
	meshList[GEO_PIER]->textureArray[0] = LoadTGA("Image//PierWood.tga");
	//GALLEY BOAT
	meshList[GEO_GALLEYBOAT] = MeshBuilder::GenerateOBJ("GALLEYBOAT", "OBJ//galleyboat.obj");
	meshList[GEO_GALLEYBOAT]->textureArray[0] = LoadTGA("Image//Galleyboat.tga");
	meshList[GEO_GALLEYBOATSAIL] = MeshBuilder::GenerateOBJ("GALLEYBOATSAIL", "OBJ//galleyboatsail.obj");
	meshList[GEO_GALLEYBOATSAIL]->textureArray[0] = LoadTGA("Image//white.tga");
	meshList[GEO_GALLEYBOATWHEEL] = MeshBuilder::GenerateOBJ("GALLEYBOATWHEEL", "OBJ//shipwheel.obj");
	meshList[GEO_GALLEYBOATWHEEL]->textureArray[0] = LoadTGA("Image//Rowboat.tga");
	//Tower
	meshList[GEO_TOWERBOTTOM] = MeshBuilder::GenerateOBJ("TOWER", "OBJ//towerbottom.obj");
	meshList[GEO_TOWERBOTTOM]->textureArray[0] = LoadTGA("Image//TowerBottom.tga");
	meshList[GEO_TOWERTOP] = MeshBuilder::GenerateOBJ("TOWER", "OBJ//towertop.obj");
	meshList[GEO_TOWERTOP]->textureArray[0] = LoadTGA("Image//TowerBrick.tga");
	//Water Fountain
	meshList[GEO_WATERFOUNTAIN] = MeshBuilder::GenerateOBJ("WATERFOUNTAIN", "OBJ//waterfountain.obj");
	meshList[GEO_WATERFOUNTAIN]->textureArray[0] = LoadTGA("Image//WaterFountain.tga");
	meshList[GEO_FOUNTAINWATER] = MeshBuilder::GenerateOBJ("FOUNTAINWATER", "OBJ//fountainwater.obj");
	meshList[GEO_FOUNTAINWATER]->textureArray[0] = LoadTGA("Image//Sea.tga");
	meshList[GEO_FOUNTAINWATER]->textureArray[1] = LoadTGA("Image//LightBlue.tga");
	
	meshList[GEO_HOUSE] = MeshBuilder::GenerateOBJ("HOUSE", "OBJ//House.obj");
	meshList[GEO_HOUSE]->textureArray[0] = LoadTGA("Image//house.tga");

	meshList[GEO_TREE] = MeshBuilder::GenerateQuad("GEO_TREE", Color(1, 1, 1), 1.f);
	meshList[GEO_TREE]->textureArray[0] = LoadTGA("Image//tree.tga");
	//Terrains & Skys
	meshList[GEO_SKYPLANE] = MeshBuilder::GenerateSkyPlane("GEO_SKYPLANE", Color(1, 1, 1), 128, 200.0f, 2000.0f, 1.0f, 1.0f);
	meshList[GEO_SKYPLANE]->textureArray[0] = LoadTGA("Image//Sky.tga");

	meshList[GEO_TERRAIN] = MeshBuilder::GenerateTerrain("GEO_TERRAIN", "Image//heightmap.raw", m_heightMap);
	meshList[GEO_TERRAIN]->textureArray[0] = LoadTGA("Image//TerrainRock.tga");
	meshList[GEO_TERRAIN]->textureArray[1] = LoadTGA("Image//TerrainMoss.tga");
	meshList[GEO_TERRAIN]->textureArray[2] = LoadTGA("Image//TerrainSand.tga");

	meshList[GEO_SEA] = MeshBuilder::GenerateQuad("GEO_SEA", Color(1, 1, 1), 1.f);
	meshList[GEO_SEA]->textureArray[0] = LoadTGA("Image//Sea.tga");

	//Particle
	meshList[GEO_PARTICLE_WATER] = MeshBuilder::GenerateQuad("GEO_PARTICLE_WATER", Color(1, 1, 1), 1.f);
	meshList[GEO_PARTICLE_WATER]->textureArray[0] = LoadTGA("Image//WaterDrop.tga");

	//Animation
	meshList[GEO_SPRITE_ANIMATION] = MeshBuilder::GenerateSpriteAnimation("dog", 1, 6);
	meshList[GEO_SPRITE_ANIMATION]->textureArray[0] = LoadTGA("Image//Dog.tga");
	SpriteAnimation *sa = dynamic_cast<SpriteAnimation*>(meshList[GEO_SPRITE_ANIMATION]);
	if (sa)
	{
		sa->m_anim = new Animation();
		sa->m_anim->Set(0, 4, 0, 1.f, true);
	}

	// Load the ground mesh and texture
	meshList[GEO_GROUND] = MeshBuilder::GenerateQuad("GROUND", Color(1, 1, 1), 1.f);
	meshList[GEO_GROUND]->textureArray[0] = LoadTGA("Image//TerrainSand.tga");
	meshList[GEO_GROUND]->textureArray[1] = LoadTGA("Image//GroundDirt.tga");
	meshList[GEO_GROUND]->textureArray[2] = LoadTGA("Image//GroundDirt2.tga");

	//Shadow GEO
	meshList[GEO_LIGHT_DEPTH_QUAD] = MeshBuilder::GenerateQuad("LIGHT_DEPTH_TEXTURE", Color(1, 1, 1), 1.f);
	meshList[GEO_LIGHT_DEPTH_QUAD]-> textureArray[0] = m_lightDepthFBO.GetTexture();

	// Projection matrix : 45� Field of View, 4:3 ratio, display range : 0.1 unit <-> 1000 units
	Mtx44 perspective;
	perspective.SetToPerspective(45.0f, 4.0f / 3.0f, 0.1f, 10000.0f);
	msProjectionStack.LoadMatrix(perspective);
	
	fRotateAngle = 0;
	bLightEnabled = true;
	dSkyRotate = 00;
	dWaterRotate = 0;
	bSky2 = false;
	bSky3 = false;
	bDogToLeft = false;
	bDogToRight = true;
	iDogRotate = 0;
	fDogPosX = 0;
	iFogEnable = 0;
	iParticleCount = 0;
	MAX_PARTICLE = 0;
	v3Gravity.Set(0, -9.8, 0);
	bIsRaining = false;
	fDense = 0.0000f;
}

void SceneText::Update(double dt)
{
	if(Application::IsKeyPressed('1'))
		glEnable(GL_CULL_FACE);
	if(Application::IsKeyPressed('2'))
		glDisable(GL_CULL_FACE);
	if(Application::IsKeyPressed('3'))
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	if(Application::IsKeyPressed('4'))
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	
	if(Application::IsKeyPressed('5'))
	{
		lLights[0].type = Light::LIGHT_POINT;
		glUniform1i(m_parameters[U_LIGHT0_TYPE], lLights[0].type);
	}
	else if(Application::IsKeyPressed('6'))
	{
		lLights[0].type = Light::LIGHT_DIRECTIONAL;
		glUniform1i(m_parameters[U_LIGHT0_TYPE], lLights[0].type);
	}
	else if(Application::IsKeyPressed('7'))
	{
		lLights[0].type = Light::LIGHT_SPOT;
		glUniform1i(m_parameters[U_LIGHT0_TYPE], lLights[0].type);
	}
	else if(Application::IsKeyPressed('8'))
	{
		bLightEnabled = true;
	}
	else if(Application::IsKeyPressed('9'))
	{
		bLightEnabled = false;
	}

	//Sky Rotation & time changing
	dSkyRotate += 0.1;
	if (dSkyRotate > 110 && bSky2 == false)//Evening
	{
		meshList[GEO_SKYPLANE]->textureArray[0] = LoadTGA("Image//Sky2.tga");
		glUniform1f(m_parameters[U_FOG_DENSITY], 0.0003f);
		iFogEnable = 1;
		bIsRaining = true;
		MAX_PARTICLE = 1000;
		bSky2 = true;
	}
	else if (dSkyRotate > 140 && bSky3 == false)//Night, 140 = 8PM
	{
		meshList[GEO_SKYPLANE]->textureArray[0] = LoadTGA("Image//Sky3.tga");
		iFogEnable = 1;
		bIsRaining = true;
		MAX_PARTICLE = 2000;
		bSky3 = true;
		lLights[0].position.Set(-40, 40, -40);
	}
	else if (dSkyRotate > 240)//Morning, 240 = 6AM
	{
		meshList[GEO_SKYPLANE]->textureArray[0] = LoadTGA("Image//Sky.tga");
		iFogEnable = 0;
		dSkyRotate = 0;
		bIsRaining = false;
		bSky2 = false;
		bSky3 = false;
		fDense = 0.0000f;
	}
	if (bSky2 == true && dSkyRotate < 180)
	{
		fDense += 0.0001f *(dt * 2);
		glUniform1f(m_parameters[U_FOG_DENSITY], fDense);
		if (fDense > 0.0012f)
		{
			fDense = 0.0012f;
		}
	}
	if (dSkyRotate > 200)
	{
		fDense -= 0.00006f *(dt * 2);
		glUniform1f(m_parameters[U_FOG_DENSITY], fDense);
		if (fDense < 0.0002f)
		{
			fDense = 0.0002f;
		}
	}
	if (bSky3 == false)
	{
		if (lLights[0].position.z < 40 && lLights[0].position.x == -40)
		{
			lLights[0].position.z += 0.2;
		}
		else if (lLights[0].position.z == 40 && lLights[0].position.x < 40)
		{
			lLights[0].position.x += 0.2;
		}
		else if (lLights[0].position.z > -40 && lLights[0].position.x == 40)
		{
			lLights[0].position.z -= 0.2;
		}
		else if (lLights[0].position.z == -40 && lLights[0].position.x > -40)
		{
			lLights[0].position.x -= 0.2;
		}
		if (lLights[0].position.z > 40)
		{
			lLights[0].position.z = 40;
		}
		else if (lLights[0].position.z < -40)
		{
			lLights[0].position.z = -40;
		}
		if (lLights[0].position.x > 40)
		{
			lLights[0].position.x = 40;
		}
		else if (lLights[0].position.x < -40)
		{
			lLights[0].position.x = -40;
		}
	}

	//Water Rotation
	dWaterRotate += 0.1;
	if (dWaterRotate > 360)
	{
		dWaterRotate = 0;
	}
	//Dog
	if (bDogToRight == true)
	{
		fDogPosX++;
		if (fDogPosX > 500)
		{
			bDogToRight = false;
			bDogToLeft = true;
			iDogRotate = 180;
		}
	}
	else if (bDogToLeft == true)
	{
		fDogPosX--;
		if (fDogPosX < 0)
		{
			bDogToRight = true;
			bDogToLeft = false;
			iDogRotate = 0;
		}
	}

	if (Application::IsKeyPressed('I'))
		lLights[0].position.z -= 1;
	if (Application::IsKeyPressed('K'))
		lLights[0].position.z += 1;
	if (Application::IsKeyPressed('J'))
		lLights[0].position.x -= 1;
	if (Application::IsKeyPressed('L'))
		lLights[0].position.x += 1;
	if (Application::IsKeyPressed('O'))
		lLights[0].position.y -= 1;
	if (Application::IsKeyPressed('P'))
		lLights[0].position.y += 1;//(float)(10.f * dt);

	fRotateAngle += (float)(10 * dt);

	camera.Update(dt);

	fFps = (int)(1.f / dt);

	SpriteAnimation *sa = dynamic_cast<SpriteAnimation*>(meshList[GEO_SPRITE_ANIMATION]);
	if (sa)
	{
		sa->Update(dt);
		sa->m_anim->animActive = true;
	}
	UpdateParticles(dt);
}

void SceneText::UpdateParticles(double dt)
{
	if (iParticleCount < MAX_PARTICLE)
	{
		ParticleObject* particle = GetParticle();
		particle->type = ParticleObject_TYPE::P_WATER;
		particle->scale.Set(15, 15, 15);
		particle->vel.Set(1, 2, 1);
		particle->rotationSpeed = Math::RandFloatMinMax(20.f, 40.f);
		particle->pos.Set(Math::RandFloatMinMax(-2000, 2000), 1400.f, Math::RandFloatMinMax(-2000, 2000));
	}

	for (std::vector<ParticleObject *>::iterator it = particleList.begin(); it != particleList.end(); ++it)
	{
		ParticleObject *particle = (ParticleObject *)*it;
		if (particle->active)
		{
			if (particle->type == ParticleObject_TYPE::P_WATER)
			{
				particle->vel += v3Gravity * (float)dt;
				particle->pos += particle->vel * (float)dt * 10.f;
				particle->rotation += particle->rotationSpeed * (float)dt;
			}
			if (particle->pos.y < -50)
			{
				particle->active = false;
				iParticleCount--;
			}
		}
	}
}

ParticleObject* SceneText::GetParticle(void)
{
	for (std::vector<ParticleObject *>::iterator it = particleList.begin(); it != particleList.end(); ++it)
	{
		ParticleObject *particle = (ParticleObject *)*it;
		if (!particle->active)
		{
			particle->active = true;
			iParticleCount++;
			return particle;
		}
	}
	for (unsigned i = 0; i < 10; ++i)
	{
		ParticleObject *particle = new ParticleObject(ParticleObject_TYPE::P_WATER);
		particleList.push_back(particle);
	}
	
	ParticleObject *particle = particleList.back();
	particle->active = true;
	iParticleCount++;
	return particle;
}

void SceneText::RenderText(Mesh* mesh, std::string text, Color color)
{
	if(!mesh || mesh->textureID <= 0)
		return;
	
	glDisable(GL_DEPTH_TEST);
	glUniform1i(m_parameters[U_TEXT_ENABLED], 1);
	glUniform3fv(m_parameters[U_TEXT_COLOR], 1, &color.r);
	glUniform1i(m_parameters[U_LIGHTENABLED], 0);
	glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED], 1);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, mesh->textureID);
	glUniform1i(m_parameters[U_COLOR_TEXTURE], 0);
	for(unsigned i = 0; i < text.length(); ++i)
	{
		Mtx44 characterSpacing;
		characterSpacing.SetToTranslation(i * 1.0f, 0, 0); //1.0f is the spacing of each character, you may change this value
		Mtx44 MVP = msProjectionStack.Top() * msViewStack.Top() * msModelStack.Top() * characterSpacing;
		glUniformMatrix4fv(m_parameters[U_MVP], 1, GL_FALSE, &MVP.a[0]);
	
		mesh->Render((unsigned)text[i] * 6, 6);
	}
	glBindTexture(GL_TEXTURE_2D, 0);
	glUniform1i(m_parameters[U_TEXT_ENABLED], 0);
	glEnable(GL_DEPTH_TEST);
}

void SceneText::RenderTextOnScreen(Mesh* mesh, std::string text, Color color, float size, float x, float y)
{
	if(!mesh || mesh->textureID <= 0)
		return;
	
	glDisable(GL_DEPTH_TEST);
	Mtx44 ortho;
	ortho.SetToOrtho(0, 80, 0, 60, -10, 10);
	msProjectionStack.PushMatrix();
		msProjectionStack.LoadMatrix(ortho);
		msViewStack.PushMatrix();
			msViewStack.LoadIdentity();
			msModelStack.PushMatrix();
				msModelStack.LoadIdentity();
				msModelStack.Translate(x, y, 0);
				msModelStack.Scale(size, size, size);
				glUniform1i(m_parameters[U_TEXT_ENABLED], 1);
				glUniform3fv(m_parameters[U_TEXT_COLOR], 1, &color.r);
				glUniform1i(m_parameters[U_LIGHTENABLED], 0);
				glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED], 1);
				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, mesh->textureID);
				glUniform1i(m_parameters[U_COLOR_TEXTURE], 0);
				for(unsigned i = 0; i < text.length(); ++i)
				{
					Mtx44 characterSpacing;
					characterSpacing.SetToTranslation(i * 1.0f + 0.5f, 0.5f, 0); //1.0f is the spacing of each character, you may change this value
					Mtx44 MVP = msProjectionStack.Top() * msViewStack.Top() * msModelStack.Top() * characterSpacing;
					glUniformMatrix4fv(m_parameters[U_MVP], 1, GL_FALSE, &MVP.a[0]);
	
					mesh->Render((unsigned)text[i] * 6, 6);
				}
				glBindTexture(GL_TEXTURE_2D, 0);
				glUniform1i(m_parameters[U_TEXT_ENABLED], 0);
			msModelStack.PopMatrix();
		msViewStack.PopMatrix();
	msProjectionStack.PopMatrix();
	glEnable(GL_DEPTH_TEST);
}

void SceneText::RenderMeshIn2D(Mesh *mesh, bool enableLight, float size, float x, float y)
{
	Mtx44 ortho;
	ortho.SetToOrtho(-80, 80, -60, 60, -10, 10);
	msProjectionStack.PushMatrix();
		msProjectionStack.LoadMatrix(ortho);
		msViewStack.PushMatrix();
			msViewStack.LoadIdentity();
			msModelStack.PushMatrix();
				msModelStack.LoadIdentity();
				msModelStack.Scale(size, size, size);
				msModelStack.Translate(x, y, 0);
       
				Mtx44 MVP, modelView, modelView_inverse_transpose;
	
				MVP = msProjectionStack.Top() * msViewStack.Top() * msModelStack.Top();
				glUniformMatrix4fv(m_parameters[U_MVP], 1, GL_FALSE, &MVP.a[0]);
				if(mesh->textureID > 0)
				{
					glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED], 1);
					glActiveTexture(GL_TEXTURE0);
					glBindTexture(GL_TEXTURE_2D, mesh->textureID);
					glUniform1i(m_parameters[U_COLOR_TEXTURE], 0);
				}
				else
				{
					glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED], 0);
				}
				mesh->Render();
				if(mesh->textureID > 0)
				{
					glBindTexture(GL_TEXTURE_2D, 0);
				}
       
			msModelStack.PopMatrix();
		msViewStack.PopMatrix();
	msProjectionStack.PopMatrix();

}

void SceneText::RenderMesh(Mesh *mesh, bool enableLight)
{
	Mtx44 MVP, modelView, modelView_inverse_transpose;

	if (m_renderPass == RENDER_PASS_PRE && bSky3 == false)
	{
		Mtx44 lightDepthMVP = m_lightDepthProj *m_lightDepthView * msModelStack.Top();
		glUniformMatrix4fv(m_parameters[U_LIGHT_DEPTH_MVP_GPASS], 1, GL_FALSE, &lightDepthMVP.a[0]);
		mesh->Render();
		return;
	}

	MVP = msProjectionStack.Top() * msViewStack.Top() * msModelStack.Top();
	glUniformMatrix4fv(m_parameters[U_MVP], 1, GL_FALSE, &MVP.a[0]);

	//new place for the prev code 
	modelView = msViewStack.Top() * msModelStack.Top();
	glUniformMatrix4fv(m_parameters[U_MODELVIEW], 1, GL_FALSE, &modelView.a[0]);

	if (enableLight && bLightEnabled)
	{
		glUniform1i(m_parameters[U_LIGHTENABLED], 1);

		modelView_inverse_transpose = modelView.GetInverse().GetTranspose();
		glUniformMatrix4fv(m_parameters[U_MODELVIEW_INVERSE_TRANSPOSE], 1, GL_FALSE, &modelView.a[0]);

		Mtx44 lightDepthMVP = m_lightDepthProj* m_lightDepthView * msModelStack.Top();
		glUniformMatrix4fv(m_parameters[U_LIGHT_DEPTH_MVP], 1, GL_FALSE, &lightDepthMVP.a[0]);

		//load material
		glUniform3fv(m_parameters[U_MATERIAL_AMBIENT], 1, &mesh->material.kAmbient.r);
		glUniform3fv(m_parameters[U_MATERIAL_DIFFUSE], 1, &mesh->material.kDiffuse.r);
		glUniform3fv(m_parameters[U_MATERIAL_SPECULAR], 1, &mesh->material.kSpecular.r);
		glUniform1f(m_parameters[U_MATERIAL_SHININESS], mesh->material.kShininess);
	}
	else
	{
		glUniform1i(m_parameters[U_LIGHTENABLED], 0);

		//load material
		glUniform3fv(m_parameters[U_MATERIAL_AMBIENT], 1, &mesh->material.kAmbient.r);
		glUniform3fv(m_parameters[U_MATERIAL_DIFFUSE], 1, &mesh->material.kDiffuse.r);
		glUniform3fv(m_parameters[U_MATERIAL_SPECULAR], 1, &mesh->material.kSpecular.r);
		glUniform1f(m_parameters[U_MATERIAL_SHININESS], mesh->material.kShininess);
	}
	for (int i = 0; i < MAX_TEXTURES; ++i) {
		if (mesh->textureArray[i] > 0) {
			glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED + i], 1);
		}
		else {
			glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED + i], 0);
		}
		glActiveTexture(GL_TEXTURE0 + i);
		glBindTexture(GL_TEXTURE_2D, mesh->textureArray[i]);
		glUniform1i(m_parameters[U_COLOR_TEXTURE + i], i);

	}
	mesh->Render();
}

void SceneText::RenderGround()
{
	msModelStack.PushMatrix();
	msModelStack.Rotate(-90, 1, 0, 0);
	msModelStack.Translate(0, -550, 22);
	msModelStack.Rotate(-90, 0, 0, 1);
	msModelStack.Scale(330.0f, 400.0f, 400.0f);

	for (int x=0; x<10; x++)
	{
		for (int z=0; z<10; z++)
		{
			msModelStack.PushMatrix();
			msModelStack.Translate(x-5.0f, z-5.0f, 0.0f);
			RenderMesh(meshList[GEO_GROUND], true);
			msModelStack.PopMatrix();
		}
	}
	msModelStack.PopMatrix();
}

void SceneText::RenderSea()
{
	msModelStack.PushMatrix();
	msModelStack.Rotate(-90, 1, 0, 0);
	msModelStack.Translate(0, 1800, -50);
	msModelStack.Rotate(-90, 0, 0, 1);
	msModelStack.Scale(1000.0f, 4000.0f, 300.0f);
	RenderMesh(meshList[GEO_SEA], true);
	msModelStack.PopMatrix();
}

void SceneText::RenderPier()
{
	//RightPier
	msModelStack.PushMatrix();
	msModelStack.Translate(700.f, -40, -1500.f);
	msModelStack.Scale(100.0f, 0.1f, 400.0f);
	msModelStack.Rotate(-90, 1, 0, 0);
	RenderMesh(meshList[GEO_PIER], true);
	msModelStack.PopMatrix();
	//RightPierRightPillar
	msModelStack.PushMatrix();
	msModelStack.Translate(752, -50, -1550);
	RenderMesh(meshList[GEO_PIER_PILLAR], true);
	msModelStack.PopMatrix();

	msModelStack.PushMatrix();
	msModelStack.Translate(752, -50 , -1700);
	RenderMesh(meshList[GEO_PIER_PILLAR], true);
	msModelStack.PopMatrix();
	//RightPierLeftPillar
	msModelStack.PushMatrix();
	msModelStack.Translate(649, -50, -1550);
	RenderMesh(meshList[GEO_PIER_PILLAR], true);
	msModelStack.PopMatrix();

	msModelStack.PushMatrix();
	msModelStack.Translate(649, -50, -1700);
	RenderMesh(meshList[GEO_PIER_PILLAR], true);
	msModelStack.PopMatrix();

	//LeftPier
	msModelStack.PushMatrix();
	msModelStack.Translate(-700.f, -40, -1500.f);
	msModelStack.Scale(100.0f, 0.1f, 400.0f);
	msModelStack.Rotate(-90, 1, 0, 0);
	RenderMesh(meshList[GEO_PIER], true);
	msModelStack.PopMatrix();
	//LeftRightPierLeftPillar
	msModelStack.PushMatrix();
	msModelStack.Translate(-649, -50, -1550);
	RenderMesh(meshList[GEO_PIER_PILLAR], true);
	msModelStack.PopMatrix();

	msModelStack.PushMatrix();
	msModelStack.Translate(-649, -50, -1700);
	RenderMesh(meshList[GEO_PIER_PILLAR], true);
	msModelStack.PopMatrix();
	//LeftPierRightPillar
	msModelStack.PushMatrix();
	msModelStack.Translate(-752, -50, -1550);
	RenderMesh(meshList[GEO_PIER_PILLAR], true);
	msModelStack.PopMatrix();

	msModelStack.PushMatrix();
	msModelStack.Translate(-752, -50, -1700);
	RenderMesh(meshList[GEO_PIER_PILLAR], true);
	msModelStack.PopMatrix();

	//MiddlePier
	msModelStack.PushMatrix();
	msModelStack.Translate(200.f, -40, -1500.f);
	msModelStack.Scale(100.0f, 0.1f, 400.0f);
	msModelStack.Rotate(-90, 1, 0, 0);
	RenderMesh(meshList[GEO_PIER], true);
	msModelStack.PopMatrix();

	msModelStack.PushMatrix();
	msModelStack.Translate(200.f, -40, -1897);
	msModelStack.Scale(100.0f, 0.1f, 400.0f);
	msModelStack.Rotate(-90, 1, 0, 0);
	RenderMesh(meshList[GEO_PIER], true);
	msModelStack.PopMatrix();
	//MiddleRightPierLeftPillar
	msModelStack.PushMatrix();
	msModelStack.Translate(251, -50, -1550);
	RenderMesh(meshList[GEO_PIER_PILLAR], true);
	msModelStack.PopMatrix();

	msModelStack.PushMatrix();
	msModelStack.Translate(251, -50, -1700);
	RenderMesh(meshList[GEO_PIER_PILLAR], true);
	msModelStack.PopMatrix();

	msModelStack.PushMatrix();
	msModelStack.Translate(251, -50, -1850);
	RenderMesh(meshList[GEO_PIER_PILLAR], true);
	msModelStack.PopMatrix();

	msModelStack.PushMatrix();
	msModelStack.Translate(251, -50, -2095);
	RenderMesh(meshList[GEO_PIER_PILLAR], true);
	msModelStack.PopMatrix();
	//MiddlePierRightPillar
	msModelStack.PushMatrix();
	msModelStack.Translate(148, -50, -1550);
	RenderMesh(meshList[GEO_PIER_PILLAR], true);
	msModelStack.PopMatrix();

	msModelStack.PushMatrix();
	msModelStack.Translate(148, -50, -1700);
	RenderMesh(meshList[GEO_PIER_PILLAR], true);
	msModelStack.PopMatrix();

	msModelStack.PushMatrix();
	msModelStack.Translate(148, -50, -1850);
	RenderMesh(meshList[GEO_PIER_PILLAR], true);
	msModelStack.PopMatrix();

	msModelStack.PushMatrix();
	msModelStack.Translate(148, -50, -2095);
	RenderMesh(meshList[GEO_PIER_PILLAR], true);
	msModelStack.PopMatrix();
}

void SceneText::RenderSkyPlane()
{
	msModelStack.PushMatrix();
	msModelStack.Translate(0, 1800, 0);
	msModelStack.Scale(1.2, 1, 1.2);
	msModelStack.Rotate(dSkyRotate,0,1,0);
	RenderMesh(meshList[GEO_SKYPLANE], false);
	msModelStack.PopMatrix();
}

void SceneText::RenderTrees()
{
	Vector3 Pos; 
	for (int i = 0; i < 15; ++i)
	{
		if (i < 4)
		{
			Pos.Set(1900 - (i * 300), 0, 200.0f);
		}
		else if (i > 3 && i < 8)
		{
			Pos.Set(1950 - ((i- 4) * 300), 0, -100.0f);
		}
		else if (i > 7 && i < 12)
		{
			Pos.Set(1950 - ((i - 8) * 300), 0, -400.0f);
		}

		else if (i > 7 && i < 15)
		{
			Pos.Set(1950 - ((i - 12) * 300), 0, -700.0f);
		}
		msModelStack.PushMatrix();
		msModelStack.Translate(Pos.x, 350 * ReadHeightMap(m_heightMap, Pos.x / 4000, Pos.z / 4000), Pos.z);
		msModelStack.Rotate(Math::RadianToDegree(atan2(camera.position.x - Pos.x, camera.position.z - Pos.z)), 0, 1, 0);
		msModelStack.Scale(250, 250, 250); 
		RenderMesh(meshList[GEO_TREE], false); 
		msModelStack.PopMatrix(); 
	}
}

void SceneText::RenderAnimation()
{
	msModelStack.PushMatrix();
	msModelStack.Translate(fDogPosX, 33.f, 0.f);
	msModelStack.Scale(25.0f, 25.0f, 25.0f);
	msModelStack.Rotate(iDogRotate, 0, 1, 0);
	RenderMesh(meshList[GEO_SPRITE_ANIMATION], false);
	msModelStack.PopMatrix();
}

void SceneText::RenderParticles(ParticleObject *particle)
{
	switch (particle->type)
	{
	case ParticleObject_TYPE::P_WATER:
		msModelStack.PushMatrix();
		msModelStack.Translate(particle->pos.x, particle->pos.y, particle->pos.z);
		msModelStack.Rotate(Math::RadianToDegree(atan2(camera.position.x - particle->pos.x, camera.position.z - particle->pos.z)), 0, 1, 0);
		//modelStack.Rotate(particle->rotation, 0, 0, 1);
		msModelStack.Scale(particle->scale.x/2, particle->scale.y / 2, particle->scale.z / 2);
		RenderMesh(meshList[GEO_PARTICLE_WATER], false);
		msModelStack.PopMatrix();
		break;
	}
}

void SceneText::RenderTerrain()
{
	msModelStack.PushMatrix();
	msModelStack.Translate(30.f, -120.f, 0.f);
	msModelStack.Scale(5000.0f, 350.0f, 4000.0f);
	RenderMesh(meshList[GEO_TERRAIN], true);
	msModelStack.PopMatrix();
}

void SceneText::RenderBuilding()
{
	msModelStack.PushMatrix();
	msModelStack.Translate(400 , 290, 140);
	msModelStack.Scale(7, 7, 7);
	RenderMesh(meshList[GEO_TOWERBOTTOM], true);
	msModelStack.PopMatrix();

	msModelStack.PushMatrix();
	msModelStack.Translate(400, 290, 140);
	msModelStack.Scale(7, 7, 7);
	RenderMesh(meshList[GEO_TOWERTOP], true);
	msModelStack.PopMatrix();

	msModelStack.PushMatrix();
	msModelStack.Translate(-1250, 290, 140);
	msModelStack.Scale(7, 7, 7);
	RenderMesh(meshList[GEO_TOWERBOTTOM], true);
	msModelStack.PopMatrix();

	msModelStack.PushMatrix();
	msModelStack.Translate(-1250, 290,140);
	msModelStack.Scale(7, 7, 7);
	RenderMesh(meshList[GEO_TOWERTOP], true);
	msModelStack.PopMatrix();

	msModelStack.PushMatrix();
	msModelStack.Translate(-400, 20, -300);
	msModelStack.Scale(2, 2, 2);
	RenderMesh(meshList[GEO_WATERFOUNTAIN], true);
	msModelStack.PopMatrix();

	msModelStack.PushMatrix();
	msModelStack.Translate(-400, 20, -300);
	msModelStack.Scale(2, 2, 2);
	msModelStack.Rotate(dWaterRotate, 0,1, 0);
	RenderMesh(meshList[GEO_FOUNTAINWATER], true);
	msModelStack.PopMatrix();

	msModelStack.PushMatrix();
	msModelStack.Translate(415, 20, -250);
	msModelStack.Scale(11, 11, 11);
	RenderMesh(meshList[GEO_HOUSE], true);
	msModelStack.PopMatrix();

	msModelStack.PushMatrix();
	msModelStack.Translate(615, 20, -600);
	msModelStack.Scale(11, 11, 11);
	RenderMesh(meshList[GEO_HOUSE], true);
	msModelStack.PopMatrix();

	msModelStack.PushMatrix();
	msModelStack.Translate(-1235, 20, -250);
	msModelStack.Scale(11, 11, 11);
	msModelStack.Rotate(-180, 0, 1, 0);
	RenderMesh(meshList[GEO_HOUSE], true);
	msModelStack.PopMatrix();

	msModelStack.PushMatrix();
	msModelStack.Translate(-1435 , 20, -600);
	msModelStack.Scale(11, 11, 11);
	msModelStack.Rotate(-180, 0, 1, 0);
	RenderMesh(meshList[GEO_HOUSE], true);
	msModelStack.PopMatrix();

	msModelStack.PushMatrix();
	msModelStack.Translate(715, 20, -1000);
	msModelStack.Scale(11, 11, 11);
	msModelStack.Rotate(-90, 0, 1, 0);
	RenderMesh(meshList[GEO_HOUSE], true);
	msModelStack.PopMatrix();

	msModelStack.PushMatrix();
	msModelStack.Translate(415, 20, -1000);
	msModelStack.Scale(11, 11, 11);
	msModelStack.Rotate(-90, 0, 1, 0);
	RenderMesh(meshList[GEO_HOUSE], true);
	msModelStack.PopMatrix();

	msModelStack.PushMatrix();
	msModelStack.Translate(115, 20, -1000);
	msModelStack.Scale(11, 11, 11);
	msModelStack.Rotate(-90, 0, 1, 0);
	RenderMesh(meshList[GEO_HOUSE], true);
	msModelStack.PopMatrix();

	msModelStack.PushMatrix();
	msModelStack.Translate(-885, 20, -1000);
	msModelStack.Scale(11, 11, 11);
	msModelStack.Rotate(-90, 0, 1, 0);
	RenderMesh(meshList[GEO_HOUSE], true);
	msModelStack.PopMatrix();

	msModelStack.PushMatrix();
	msModelStack.Translate(-1185, 20, -1000);
	msModelStack.Scale(11, 11, 11);
	msModelStack.Rotate(-90, 0, 1, 0);
	RenderMesh(meshList[GEO_HOUSE], true);
	msModelStack.PopMatrix();

	msModelStack.PushMatrix();
	msModelStack.Translate(-1485, 20, -1000);
	msModelStack.Scale(11, 11, 11);
	msModelStack.Rotate(-90, 0, 1, 0);
	RenderMesh(meshList[GEO_HOUSE], true);
	msModelStack.PopMatrix();
}

void SceneText::RenderSeaOBJ()
{
	//RightPierRightBoats
	msModelStack.PushMatrix();
	msModelStack.Translate(800, -47, -1575);
	RenderMesh(meshList[GEO_ROWBOAT], true);
	msModelStack.PopMatrix();

	msModelStack.PushMatrix();
	msModelStack.Translate(800, -47, -1625);
	RenderMesh(meshList[GEO_ROWBOAT], true);
	msModelStack.PopMatrix();

	msModelStack.PushMatrix();
	msModelStack.Translate(800, -47, -1675 );
	RenderMesh(meshList[GEO_ROWBOAT], true);
	msModelStack.PopMatrix();
	//RightPierLeftBoats
	msModelStack.PushMatrix();
	msModelStack.Translate(600, -47, -1575);
	RenderMesh(meshList[GEO_ROWBOAT], true);
	msModelStack.PopMatrix();

	msModelStack.PushMatrix();
	msModelStack.Translate(600, -47, -1625);
	RenderMesh(meshList[GEO_ROWBOAT], true);
	msModelStack.PopMatrix();

	msModelStack.PushMatrix();
	msModelStack.Translate(600, -47, -1675);
	RenderMesh(meshList[GEO_ROWBOAT], true);
	msModelStack.PopMatrix();
	//LeftPierLeftBoats
	msModelStack.PushMatrix();
	msModelStack.Translate(-800, -47, -1575);
	RenderMesh(meshList[GEO_ROWBOAT], true);
	msModelStack.PopMatrix();

	msModelStack.PushMatrix();
	msModelStack.Translate(-800, -47, -1625);
	RenderMesh(meshList[GEO_ROWBOAT], true);
	msModelStack.PopMatrix();

	msModelStack.PushMatrix();
	msModelStack.Translate(-800, -47, -1675);
	RenderMesh(meshList[GEO_ROWBOAT], true);
	msModelStack.PopMatrix();
	//LeftPierRightBoats
	msModelStack.PushMatrix();
	msModelStack.Translate(-500, -45, -1775);
	msModelStack.Rotate(40, 0, 1, 0);
	RenderMesh(meshList[GEO_ROWBOAT], true);
	msModelStack.PopMatrix();

	msModelStack.PushMatrix();
	msModelStack.Translate(-600, -47, -1575);
	RenderMesh(meshList[GEO_ROWBOAT], true);
	msModelStack.PopMatrix();

	msModelStack.PushMatrix();
	msModelStack.Translate(-600, -47, -1625);
	RenderMesh(meshList[GEO_ROWBOAT], true);
	msModelStack.PopMatrix();
	//Galley
	msModelStack.PushMatrix();
	msModelStack.Translate(10, -30, -1825);
	RenderMesh(meshList[GEO_GALLEYBOAT], true);
	msModelStack.PopMatrix();

	msModelStack.PushMatrix();
	msModelStack.Translate(10, 100, -1775);
	msModelStack.Scale(1, 5, 1);
	RenderMesh(meshList[GEO_PIER_PILLAR], true);
	msModelStack.PopMatrix();

	msModelStack.PushMatrix();
	msModelStack.Translate(10, 150, -1770);
	msModelStack.Rotate(90, 0, 0, 1);
	msModelStack.Scale(1, 3, 1);
	RenderMesh(meshList[GEO_PIER_PILLAR], true);
	msModelStack.PopMatrix();

	msModelStack.PushMatrix();
	msModelStack.Translate(10, 80, -1940);
	msModelStack.Scale(1, 5, 1);
	RenderMesh(meshList[GEO_PIER_PILLAR], true);
	msModelStack.PopMatrix();

	msModelStack.PushMatrix();
	msModelStack.Translate(10, 130, -1935);
	msModelStack.Rotate(90, 0, 0, 1);
	msModelStack.Scale(1, 3, 1);
	RenderMesh(meshList[GEO_PIER_PILLAR], true);
	msModelStack.PopMatrix();
	
	msModelStack.PushMatrix();
	msModelStack.Translate(18, 130, -1759);
	msModelStack.Rotate(90, 0, 1, 0);
	msModelStack.Scale(10, 30, 35);
	RenderMesh(meshList[GEO_GALLEYBOATSAIL], true);
	msModelStack.PopMatrix();

	msModelStack.PushMatrix();
	msModelStack.Translate(18, 118, -1925);
	msModelStack.Rotate(90, 0, 1, 0);
	msModelStack.Scale(10, 20, 35);
	RenderMesh(meshList[GEO_GALLEYBOATSAIL], true);
	msModelStack.PopMatrix();

	msModelStack.PushMatrix();
	msModelStack.Translate(10, 57, -1902);
	msModelStack.Scale(0.25, 0.25, 0.25);
	RenderMesh(meshList[GEO_GALLEYBOATWHEEL], true);
	msModelStack.PopMatrix();

}


void SceneText::RenderWorld()
{
	RenderGround();
	RenderPier();
	RenderSea();
	RenderSkyPlane();

	RenderTerrain();
	RenderTrees();
	RenderBuilding();
	RenderSeaOBJ();
	RenderAnimation();
	if (bIsRaining)
	{
		for (std::vector<ParticleObject *>::iterator it = particleList.begin(); it != particleList.end(); ++it)
		{
			ParticleObject *particle = (ParticleObject *)*it;
			if (particle->active)
			{
				RenderParticles(particle);
			}
		}
	}
}

void SceneText::RenderPassGPass()
{
	m_renderPass = RENDER_PASS_PRE;
	m_lightDepthFBO.BindForWriting();
	glDisable(GL_CULL_FACE);
	glCullFace(GL_FRONT);
	glClear(GL_DEPTH_BUFFER_BIT);
	glUseProgram(m_gPassShaderID);
	////These matrices should change when light position or direction changes
	if (lLights[0].type == Light::LIGHT_DIRECTIONAL)
	{
		m_lightDepthProj.SetToOrtho(-1000, 1000, -1000, 1000, -1000, 2000);
	}
	else
	{
		m_lightDepthProj.SetToPerspective(90, 1.f, 0.1, 200);
	}
	m_lightDepthView.SetToLookAt(lLights[0].position.x, lLights[0].position.y, lLights[0].position.z, 0, 0, 0, 0, 1, 0);
	RenderWorld();
}

void SceneText::RenderPassMain()
{
	m_renderPass = RENDER_PASS_MAIN;

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, 800, 600);
	glDisable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(m_programID);
	m_lightDepthFBO.BindForReading(GL_TEXTURE8);
	glUniform1i(m_parameters[U_SHADOW_MAP], 8);

	Mtx44 perspective;
	perspective.SetToPerspective(45.0f, 4.0f / 3.0f, 0.1f, 10000.0f);
	//perspective.SetToOrtho(-80, 80, -60, 60, -1000, 1000);
	msProjectionStack.LoadMatrix(perspective);

	// Camera matrix
	msViewStack.LoadIdentity();
	msViewStack.LookAt(
		camera.position.x, camera.position.y, camera.position.z,
		camera.target.x, camera.target.y, camera.target.z,
		camera.up.x, camera.up.y, camera.up.z
	);
	
	if (lLights[0].type == Light::LIGHT_DIRECTIONAL)
	{
		Vector3 lightDir(lLights[0].position.x, lLights[0].position.y, lLights[0].position.z);
		Vector3 lightDirection_cameraspace = msViewStack.Top() * lightDir;
		glUniform3fv(m_parameters[U_LIGHT0_POSITION], 1, &lightDirection_cameraspace.x);
	}
	else if (lLights[0].type == Light::LIGHT_SPOT)
	{
		Position lightPosition_cameraspace = msViewStack.Top() * lLights[0].position;
		glUniform3fv(m_parameters[U_LIGHT0_POSITION], 1, &lightPosition_cameraspace.x);
		Vector3 spotDirection_cameraspace = msViewStack.Top() * lLights[0].spotDirection;
		glUniform3fv(m_parameters[U_LIGHT0_SPOTDIRECTION], 1, &spotDirection_cameraspace.x);
	}
	else
	{
		Position lightPosition_cameraspace = msViewStack.Top() * lLights[0].position;
		glUniform3fv(m_parameters[U_LIGHT0_POSITION], 1, &lightPosition_cameraspace.x);
	}
	if (lLights[1].type == Light::LIGHT_DIRECTIONAL)
	{
		Vector3 lightDir(lLights[1].position.x, lLights[1].position.y, lLights[1].position.z);
		Vector3 lightDirection_cameraspace = msViewStack.Top() * lightDir;
		glUniform3fv(m_parameters[U_LIGHT1_POSITION], 1, &lightDirection_cameraspace.x);
	}
	else if (lLights[1].type == Light::LIGHT_SPOT)
	{
		Position lightPosition_cameraspace = msViewStack.Top() * lLights[1].position;
		glUniform3fv(m_parameters[U_LIGHT1_POSITION], 1, &lightPosition_cameraspace.x);
		Vector3 spotDirection_cameraspace = msViewStack.Top() * lLights[1].spotDirection;
		glUniform3fv(m_parameters[U_LIGHT1_SPOTDIRECTION], 1, &spotDirection_cameraspace.x);
	}
	else
	{
		Position lightPosition_cameraspace = msViewStack.Top() * lLights[1].position;
		glUniform3fv(m_parameters[U_LIGHT1_POSITION], 1, &lightPosition_cameraspace.x);
	}
	RenderMesh(meshList[GEO_AXES], false);

	glUniform1i(m_parameters[U_FOG_ENABLED], iFogEnable);
	RenderWorld();
	glUniform1i(m_parameters[U_FOG_ENABLED], 0);

	std::ostringstream ss;
	ss.precision(5);
	ss << "FPS:" << fFps;
	RenderTextOnScreen(meshList[GEO_TEXT], ss.str(), Color(0, 1, 0), 3, 60, 55);

	ss.str("");
	ss.precision(4);
	if (dSkyRotate < 180)
	{
		if (dSkyRotate > 40)
			ss << (int)(dSkyRotate / 10 + 6) << ":00";
		else
			ss << "0" << (int)(dSkyRotate / 10 + 6) << ":00";
	}
	else
		ss << "0" << (int)(dSkyRotate / 10 - 18) << ":00";
	RenderTextOnScreen(meshList[GEO_TEXT], ss.str(), Color(0, 1, 0), 3, 33, 55);

}
void SceneText::Render()
{
	//******************************* PRE RENDER PASS*************************************
	RenderPassGPass();
	//******************************* MAIN RENDER PASS************************************
	RenderPassMain();
}
void SceneText::Exit()
{
	// Cleanup VBO
	for(int i = 0; i < NUM_GEOMETRY; ++i)
	{
		if(meshList[i])
			delete meshList[i];
	}
	while (particleList.size() > 0)
	{
		ParticleObject *particle = particleList.back();
		delete particle;
		particleList.pop_back();
	}
	glDeleteProgram(m_programID);
	glDeleteVertexArrays(1, &m_vertexArrayID);
	glDeleteProgram(m_gPassShaderID);
}
